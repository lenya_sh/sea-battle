import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { LoginOrCreateUserDTO } from "./dto/loginOrCreateUser.dto";
import { UserEntity } from "./user.entity";
import {CustomException} from "../exceptions/custom.exception";

@Injectable()
export class UsersService {
	constructor(
		@InjectRepository(UserEntity)
		private usersRepository: Repository<UserEntity>,
	) { }

	public async loginOrCreateUser(request: LoginOrCreateUserDTO.LoginOrCreateUserRequest): Promise<LoginOrCreateUserDTO.LoginOrCreateUserResponse> {
		const existedUser = await this.usersRepository.findOne({ where: { username: request.username } });
		if (existedUser) 
			return new LoginOrCreateUserDTO.LoginOrCreateUserResponse(existedUser);

		const user = this.usersRepository.create({username: request.username});
		
		const savedUser = await this.usersRepository.save(user);
		if (!savedUser) {
			throw CustomException.internalError('Error upon saving user');
		}

		return new LoginOrCreateUserDTO.LoginOrCreateUserResponse(savedUser);
	}
	
	public async getUserById(userId: number): Promise<UserEntity> {
		const user = await this.usersRepository.findOne({
			where: {
				id: userId
			}
		});
		
		if (!user)
			throw CustomException.invalidData(`Пользователь с id ${userId} не найден`);
		
		return user;
	}
}