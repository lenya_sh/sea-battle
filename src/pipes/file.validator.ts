﻿import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { CustomException } from "../exceptions/custom.exception";

@Injectable()
export class FileValidationPipe implements PipeTransform {
	transform(value: Express.Multer.File, metadata: ArgumentMetadata) {
		if (value.mimetype != 'text/csv')
			throw CustomException.invalidData('Некорректный формат файла');

		if (value.size > 10485760)
			throw CustomException.invalidData('Слишком большой файл');

		return value;
	}
}
