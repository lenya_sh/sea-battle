import {ApiProperty} from '@nestjs/swagger';
import {
	Column,
	Entity, JoinColumn,
	ManyToOne, OneToOne,
	PrimaryGeneratedColumn,
} from 'typeorm';
import {UserEntity} from "../users/user.entity";
import {SessionEntity} from "./session.entity";

@Entity({ name: 'sessions_users' })
export class SessionUsersEntity {
	@ApiProperty()
	@PrimaryGeneratedColumn()
	public id!: number;

	@ApiProperty()
	@Column({name: 'first_player_id', type: 'int'})
	public firstPlayerId!: number;

	@ManyToOne(() => UserEntity, user => user.id)
	@JoinColumn({ name: 'first_player_id' })
	@ApiProperty({ type: () => UserEntity})
	public firstPlayer!: UserEntity;

	@ApiProperty()
	@Column({ name: 'second_player_id', type: 'int' })
	public secondPlayerId!: number;

	@ManyToOne(() => UserEntity, user => user.id)
	@JoinColumn({ name: 'second_player_id' })
	@ApiProperty({ type: () => UserEntity})
	public secondPlayer!: UserEntity;

	@ApiProperty()
	@Column({name: 'session_id', type: 'int'})
	public sessionId!: number;

	@OneToOne(() => SessionEntity, session => session.sessionUsers)
	@JoinColumn({ name: 'session_id' })
	@ApiProperty({ type: () => SessionEntity})
	public session!: SessionEntity;
}