import {
	Body, Controller, Post, UsePipes,
	ValidationPipe
} from "@nestjs/common";
import { ApiBody, ApiTags } from "@nestjs/swagger";
import { LoginOrCreateUserDTO } from "./dto/loginOrCreateUser.dto";
import { UsersService } from "./users.service";

@Controller('users')
@ApiTags('Пользователи')
export class UsersController {
	constructor(private readonly usersService: UsersService) { }

	@Post('/login')
	@UsePipes(ValidationPipe)
	@ApiBody({
		description: 'Вход в учетную запись пользователя или ее создание',
		type: LoginOrCreateUserDTO.LoginOrCreateUserResponse,
	})
	loginOrCreateUser(@Body() body: LoginOrCreateUserDTO.LoginOrCreateUserRequest) {
		return this.usersService.loginOrCreateUser(body);
	}
}