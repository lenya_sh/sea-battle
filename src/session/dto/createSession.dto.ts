import {ApiProperty} from "@nestjs/swagger";
import {SessionEntity} from "../session.entity";

export namespace CreateSessionDto {
	export class CreateSessionRequest {
		@ApiProperty()
		public firstPlayerId: number;

		@ApiProperty()
		public secondPlayerId: number;
		
		constructor(firstPlayerId: number, secondPlayerId: number) {
			this.firstPlayerId = firstPlayerId;
			this.secondPlayerId = secondPlayerId;
		}
	}
	
	export class CreateSessionResponse {
		@ApiProperty()
		public session: SessionEntity;
		
		constructor(session: SessionEntity) {
			this.session = session;
		}
	}
}