import {
	Body,
	Controller,
	Get,
	Param,
	Post,
	Put
} from "@nestjs/common";
import {ApiResponse, ApiTags} from "@nestjs/swagger";
import SessionService from "./session.service";
import {GetSessionByUserIdDto} from "./dto/getSessionByUserId.dto";
import {PlaceShipsDto} from "./dto/placeShips.dto";
import {MakeMoveDto} from "./dto/makeMove.dto";
import {FindSessionDto} from "./dto/findSession.dto";
import {GetSessionStatusDto} from "./dto/getSessionStatus.dto";

@Controller('sessions')
@ApiTags('Игровые сессии')
export class SessionController {
	constructor(private sessionService: SessionService) { }

	@Get(':userId')
	@ApiResponse({
		description: 'Получить активную игровую сессию по id участника\n'
			+ 'Статусы активной сессии: Старт, Первый или второй расставили корабли, Первый или второй ходит, Пауза',
		status: 200,
		type: GetSessionByUserIdDto.GetSessionByUserIdResponse,
	})
	public async getActiveSessionByUserId(@Param() param: any) {
		return this.sessionService.getActiveSessionByUserId(new GetSessionByUserIdDto.GetSessionByUserIdRequest(param.userId));
	}

	@Get('/status/:userId')
	@ApiResponse({
		description: 'Получить игровую сессию по id участника\n',
		status: 200,
		type: GetSessionStatusDto.GetSessionStatusResponse,
	})
	public async getSessionStatus(@Param() param: any) {
		return this.sessionService.getSessionStatus(new GetSessionStatusDto.GetSessionStatusRequest(param.userId));
	}

	/*	
	@Post()
	@ApiResponse({
		description: 'Создать игру',
		status: 200,
		type: CreateSessionDto.CreateSessionResponse,
	})
	public async createSession(@Body() body: CreateSessionDto.CreateSessionRequest) {
		return this.sessionService.createSession(body);
	}
	*/

	@Post()
	@ApiResponse({
		description: 'Искать игровую сессию',
		status: 200,
		type: FindSessionDto.FindSessionResponse,
	})
	public async findSession(@Body() body: FindSessionDto.FindSessionRequest) {
		return this.sessionService.findSession(body);
	}

	@Put(':id/place-ship')
	@ApiResponse({
		description: 'Установить корабли',
		status: 200,
		type: PlaceShipsDto.PlaceShipsResponse,
	})
	public async placeShips(@Param() param: any, @Body() body: PlaceShipsDto.PlaceShipsRequest) {
		const request = new PlaceShipsDto.PlaceShipsRequest(param.id, body.playerId, body.coords);

		return this.sessionService.placeShips(request);
	}

	@Put(':id/make-move')
	@ApiResponse({
		description: 'Выполнить ход',
		status: 200,
		type: MakeMoveDto.MakeMoveResponse,
	})
	public async makeMove(@Param() param: any, @Body() body: MakeMoveDto.MakeMoveRequest) {
		const request = new MakeMoveDto.MakeMoveRequest(param.id, body.playerId, body.move);
		return this.sessionService.makeMove(request);
	}
}