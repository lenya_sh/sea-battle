import {ApiProperty} from "@nestjs/swagger";
import {IShipCoords} from "../../constants";
import {IsArray, IsDefined, IsInt} from "class-validator";
import {SessionEntity} from "../session.entity";

export namespace PlaceShipsDto {
	export class PlaceShipsRequest {
		public sessionId: number;
		
		@ApiProperty()
		@IsDefined()
		@IsInt()
		public playerId: number;
		
		@ApiProperty()
		@IsDefined()
		@IsArray()
		public coords: IShipCoords[];
		
		constructor(sessionId: number, playerId: number, coords: IShipCoords[]) {
			this.sessionId = sessionId;
			this.playerId = playerId;
			this.coords = coords;
		}
	}

	export class PlaceShipsResponse {
		@ApiProperty()
		public session: SessionEntity;
		
		constructor(session: SessionEntity) {
			this.session = session;
		}
	}
}