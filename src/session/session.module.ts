import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {SessionEntity} from "./session.entity";
import SessionService from "./session.service";
import {SessionController} from "./session.controller";
import {SessionUsersEntity} from "./sessionUsers.entity";
import {UsersModule} from "../users/users.module";
import {LookingForSessionEntity} from "./lookingForSession.entity";

@Module({
	imports: [
		TypeOrmModule.forFeature([SessionEntity]),
		TypeOrmModule.forFeature([SessionUsersEntity]),
		TypeOrmModule.forFeature([LookingForSessionEntity]),
		UsersModule
	],
	exports: [SessionService],
	providers: [SessionService],
	controllers: [SessionController]
})
export class SessionModule {}
