import { ApiProperty } from "@nestjs/swagger";
import { IsDefined, IsString } from "class-validator";
import { UserEntity } from "../user.entity";

export namespace LoginOrCreateUserDTO {
	export class LoginOrCreateUserRequest {
		@IsDefined({
			message: 'Username is defined'
		})
		@IsString({
			message: 'Username must be a string'
		})
		@ApiProperty()
		username: string;

		constructor(username: string) {
			this.username = username;
		}
	}

	export class LoginOrCreateUserResponse {
		@ApiProperty({type: [UserEntity]})
		user: UserEntity;

		constructor(user: UserEntity) {
			this.user = user;
		}
	}
}