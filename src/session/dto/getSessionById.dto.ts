import {ApiProperty} from "@nestjs/swagger";
import {SessionEntity} from "../session.entity";

export namespace GetSessionByIdDto {
	export class GetSessionByIdRequest {
		public sessionId: number;
		
		public expand?: boolean
		
		constructor(sessionId: number, expand?: boolean) {
			this.sessionId = sessionId;
			this.expand = expand;
		}
	}
	
	export class GetSessionByIdResponse {
		@ApiProperty()
		public session: SessionEntity;
		
		constructor(session: SessionEntity) {
			this.session = session;
		}
	}
}