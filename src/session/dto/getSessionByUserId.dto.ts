import {ApiProperty} from "@nestjs/swagger";
import {SessionEntity} from "../session.entity";

export namespace GetSessionByUserIdDto {
	export class GetSessionByUserIdRequest {
		public userId: number;
		
		constructor(userId: number) {
			this.userId = userId;
		}
	}
	
	export class GetSessionByUserIdResponse {
		@ApiProperty({type: SessionEntity})
		public session: SessionEntity;
		
		constructor(session: SessionEntity) {
			this.session = session;
		}
	}
}