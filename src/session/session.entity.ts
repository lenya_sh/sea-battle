import {ApiProperty} from '@nestjs/swagger';
import {
	Column,
	CreateDateColumn,
	Entity, OneToOne,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';
import {SessionUsersEntity} from "./sessionUsers.entity";
import {GameSessionStatus, IGameFieldCell} from "../constants";

@Entity({ name: 'sessions' })
export class SessionEntity {
	@ApiProperty()
	@PrimaryGeneratedColumn()
	public id!: number;
	
	@ApiProperty()
	@Column({
		type: 'varchar',
		length: 40,
		default: GameSessionStatus.Start,
	})
	status!: GameSessionStatus;

	@ApiProperty()
	@Column('varchar', { length: '7120', name: 'first_player_field', default: '[[]]', transformer: {
			from: (dbValue: string) => {
				if (!dbValue) return [[]];
				return typeof dbValue == 'object' ? dbValue : JSON.parse(dbValue);
			},
			to: (entityValue: IGameFieldCell[][]) => {
				if (!entityValue) return '[[]]';
				return JSON.stringify(entityValue);
			}
		}})
	firstPlayerField!: IGameFieldCell[][];

	@ApiProperty()
	@Column('varchar', { length: '7120', name: 'second_player_field', default: '[[]]', transformer: {
			from: (dbValue: string) => {
				if (!dbValue) return [[]];
				return typeof dbValue == 'object' ? dbValue : JSON.parse(dbValue);
			},
			to: (entityValue: IGameFieldCell[][]) => {
				if (!entityValue) return '[[]]';
				return JSON.stringify(entityValue);
			}
		}})
	secondPlayerField!: IGameFieldCell[][];

	@OneToOne(() => SessionUsersEntity, sessionUsers => sessionUsers.session)
	@ApiProperty({ type: () => SessionUsersEntity})
	public sessionUsers!: SessionUsersEntity;

	@ApiProperty()
	@CreateDateColumn({ type: 'timestamp', name: 'created_at' })
	readonly createdAt!: Date;

	@ApiProperty()
	@UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
	readonly updatedAt!: Date;
}