import {ApiProperty} from "@nestjs/swagger";
import {IsDefined, IsInt} from "class-validator";
import {IShipCoords, MoveResult} from "../../constants";

export namespace MakeMoveDto {
	export class MakeMoveRequest {
		public sessionId: number;
		
		@ApiProperty()
		@IsDefined()
		@IsInt()
		public playerId: number;
		
		@ApiProperty({example: '{letter: "A", number: 1}'})
		@IsDefined()
		public move: IShipCoords;
		
		constructor(sessionId: number, playerId: number, move: IShipCoords) {
			this.sessionId = sessionId;
			this.playerId = playerId;
			this.move = move;
		}
	}
	
	export class MakeMoveResponse {
		@ApiProperty({example: 'miss or hit or kill or win'})
		public result: MoveResult;
		
		constructor(result: MoveResult) {
			this.result = result;
		}
	}
}