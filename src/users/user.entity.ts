import { ApiProperty } from '@nestjs/swagger';
import {
	Column,
	CreateDateColumn,
	Entity, OneToMany,
	PrimaryGeneratedColumn,
	UpdateDateColumn
} from "typeorm";
import {SessionUsersEntity} from "../session/sessionUsers.entity";
import {LookingForSessionEntity} from "../session/lookingForSession.entity";

@Entity({ name: 'users' })
export class UserEntity {
	@ApiProperty()
	@PrimaryGeneratedColumn()
	public id!: number;

	@ApiProperty()
	@Column('varchar', { unique: true, length: '255' })
	username!: string;

	@OneToMany(() => SessionUsersEntity, session => session.firstPlayer)
	@ApiProperty()
	public sessionsAsFirstPlayer!: SessionUsersEntity[];

	@OneToMany(() => SessionUsersEntity, session => session.secondPlayer)
	@ApiProperty()
	public sessionsAsSecondPlayer!: SessionUsersEntity[];

	@OneToMany(() => LookingForSessionEntity, lookingForSession => lookingForSession.user)
	@ApiProperty()
	public lookingForSession!: LookingForSessionEntity[];
	
	@ApiProperty()
	@CreateDateColumn({ type: 'timestamp', name: 'created_at' })
	public readonly createdAt!: Date;

	@ApiProperty()
	@UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
	public readonly updatedAt!: Date;
}