import {Module} from '@nestjs/common';
import {ConfigModule} from '@nestjs/config';
import * as Joi from '@hapi/joi';
import {DatabaseModule} from './database/database.module';
import {UsersModule} from './users/users.module';
import {AppController} from './app.controller';
import {ScheduleModule} from '@nestjs/schedule';
import {SessionModule} from "./session/session.module";
import {AppGateway} from "./app.gateway";

@Module({
	imports: [
		ConfigModule.forRoot({
			validationSchema: Joi.object({
				MYSQL_HOST: Joi.string().required(),
				MYSQL_PORT: Joi.number().required(),
				MYSQL_USER: Joi.string().required(),
				MYSQL_PASSWORD: Joi.string().required(),
				MYSQL_DATABASE: Joi.string().required()
			}),
		}),
		ScheduleModule.forRoot(),
		DatabaseModule,
		UsersModule,
      	SessionModule
	],
	controllers: [AppController],
	providers: [
		//AppGateway
		
	],
})
export class AppModule {
}