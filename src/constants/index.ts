export enum GameSessionStatus {
	Start = 'start',
	FirstPlacedShips = 'firstPlacedShips',
	SecondPlacedShips = 'secondPlacedShips',
	FirstMove = 'firstMove',
	SecondMove = 'secondMove',
	FirstWon = 'firstWon',
	SecondWon = 'secondWon',
	Pause = 'pause',
	Error = 'error',
	Canceled = 'canceled'
}

export enum GameCellStatus {
	AvailableCell = 'availableCell',
	MissedShotCell = 'missedShotCell',
	HittedShotCell = 'hittedShotCell',
	ShipCell = 'shipCell'
}

export interface IGameFieldCell {
	coords: IShipCoords,
	status: GameCellStatus;
}

export interface IShipCoords {
	letter: LetterCoordinate;
	number: number;
}

export type MoveResult = 'miss' | 'hit' | 'kill' | 'win';

export type LetterCoordinate = 'A' | 'B' | 'C' | 'D' | 'E' | 'F';

export class Constants {
	public static CountOfShips = 7;
	public static MaxNumberCoordinate = 6;
	
	public static LetterCoordinate: LetterCoordinate[] = ['A', 'B', 'C', 'D', 'E', 'F']
	
	public static GameSessionStatus: GameSessionStatus[] = [
		GameSessionStatus.Start,
		GameSessionStatus.FirstPlacedShips,
		GameSessionStatus.SecondPlacedShips,
		GameSessionStatus.FirstMove,
		GameSessionStatus.SecondMove,
		GameSessionStatus.FirstWon,
		GameSessionStatus.SecondWon,
		GameSessionStatus.Pause,
		GameSessionStatus.Error,
		GameSessionStatus.Canceled
	];
}