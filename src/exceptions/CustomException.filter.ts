﻿import { ArgumentsHost, Catch, HttpException, HttpStatus} from "@nestjs/common";
import { CustomException } from "./custom.exception";

@Catch()
export class CustomExceptionFilter implements CustomExceptionFilter {
    public catch(exception: Error, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        let status = HttpStatus.INTERNAL_SERVER_ERROR;
        
        if (exception instanceof CustomException) {
            // если возникает непредвиденная 500 ошибка, то корректный статус ошибки находится внутри details
            status = !exception.details.status ? exception.status : exception.details.status;
        }
        else if (exception instanceof HttpException)
            status = exception.getStatus();

        response.status(status).json({ 
            statusCode: status,
            message: exception.message
        });
    }
}