import { Controller, Get, HttpStatus, Res } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';

@Controller('system')
@ApiTags('application')
export class AppController {
  @Get('/healthcheck')
  index(@Res() res: Response) {
    res.status(HttpStatus.OK).send('ok');
  }
}