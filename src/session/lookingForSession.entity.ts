import { ApiProperty } from '@nestjs/swagger';
import {
	Column,
	CreateDateColumn,
	Entity, JoinColumn, ManyToOne,
	PrimaryGeneratedColumn,
	UpdateDateColumn
} from "typeorm";
import {UserEntity} from "../users/user.entity";

@Entity({ name: 'looking_for_session' })
export class LookingForSessionEntity {
	@ApiProperty()
	@PrimaryGeneratedColumn()
	public id!: number;
	
	@ApiProperty()
	@Column('boolean', {name: 'active', default: true})
	public active!: boolean;
	
	@ApiProperty()
	@Column({ name: 'user_id', type: 'int' })
	public userId!: number;

	@ManyToOne(() => UserEntity, user => user.id)
	@JoinColumn({name: 'user_id'})
	@ApiProperty()
	public user!: UserEntity;

	@ApiProperty()
	@CreateDateColumn({ type: 'timestamp', name: 'created_at' })
	public readonly createdAt!: Date;

	@ApiProperty()
	@UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
	public readonly updatedAt!: Date;
}