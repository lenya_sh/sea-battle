import {MigrationInterface, QueryRunner} from "typeorm";

export class init1678989574105 implements MigrationInterface {
    name = 'init1678989574105'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`sessions\` (\`id\` int NOT NULL AUTO_INCREMENT, \`status\` varchar(40) NOT NULL DEFAULT 'start', \`first_player_field\` varchar(5120) NOT NULL DEFAULT '[[]]', \`second_player_field\` varchar(5120) NOT NULL DEFAULT '[[]]', \`created_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`sessions_users\` (\`id\` int NOT NULL AUTO_INCREMENT, \`first_player_id\` int NOT NULL, \`second_player_id\` int NOT NULL, \`session_id\` int NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`users\` (\`id\` int NOT NULL AUTO_INCREMENT, \`username\` varchar(255) NOT NULL, \`created_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), UNIQUE INDEX \`IDX_fe0bb3f6520ee0469504521e71\` (\`username\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`looking_for_session\` (\`id\` int NOT NULL AUTO_INCREMENT, \`active\` tinyint NOT NULL DEFAULT 1, \`user_id\` int NOT NULL, \`created_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`sessions_users\` ADD CONSTRAINT \`FK_b9d27716d00774f164e966d14fa\` FOREIGN KEY (\`first_player_id\`) REFERENCES \`users\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`sessions_users\` ADD CONSTRAINT \`FK_7974fbc5225fbd46cb1c680991d\` FOREIGN KEY (\`second_player_id\`) REFERENCES \`users\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`sessions_users\` ADD CONSTRAINT \`FK_85ca3582c13beb38be603599c5e\` FOREIGN KEY (\`session_id\`) REFERENCES \`sessions\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`looking_for_session\` ADD CONSTRAINT \`FK_5b60d86fbb39602cf8ee13c59e7\` FOREIGN KEY (\`user_id\`) REFERENCES \`users\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`looking_for_session\` DROP FOREIGN KEY \`FK_5b60d86fbb39602cf8ee13c59e7\``);
        await queryRunner.query(`ALTER TABLE \`sessions_users\` DROP FOREIGN KEY \`FK_85ca3582c13beb38be603599c5e\``);
        await queryRunner.query(`ALTER TABLE \`sessions_users\` DROP FOREIGN KEY \`FK_7974fbc5225fbd46cb1c680991d\``);
        await queryRunner.query(`ALTER TABLE \`sessions_users\` DROP FOREIGN KEY \`FK_b9d27716d00774f164e966d14fa\``);
        await queryRunner.query(`DROP TABLE \`looking_for_session\``);
        await queryRunner.query(`DROP INDEX \`IDX_fe0bb3f6520ee0469504521e71\` ON \`users\``);
        await queryRunner.query(`DROP TABLE \`users\``);
        await queryRunner.query(`DROP TABLE \`sessions_users\``);
        await queryRunner.query(`DROP TABLE \`sessions\``);
    }

}
