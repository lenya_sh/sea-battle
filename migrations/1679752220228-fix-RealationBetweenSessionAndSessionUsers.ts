import {MigrationInterface, QueryRunner} from "typeorm";

export class fixRealationBetweenSessionAndSessionUsers1679752220228 implements MigrationInterface {
    name = 'fixRealationBetweenSessionAndSessionUsers1679752220228'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`sessions\` DROP COLUMN \`first_player_field\``);
        await queryRunner.query(`ALTER TABLE \`sessions\` ADD \`first_player_field\` varchar(7120) NOT NULL DEFAULT '[[]]'`);
        await queryRunner.query(`ALTER TABLE \`sessions\` DROP COLUMN \`second_player_field\``);
        await queryRunner.query(`ALTER TABLE \`sessions\` ADD \`second_player_field\` varchar(7120) NOT NULL DEFAULT '[[]]'`);
        await queryRunner.query(`ALTER TABLE \`sessions_users\` DROP FOREIGN KEY \`FK_85ca3582c13beb38be603599c5e\``);
        await queryRunner.query(`ALTER TABLE \`sessions_users\` ADD UNIQUE INDEX \`IDX_85ca3582c13beb38be603599c5\` (\`session_id\`)`);
        await queryRunner.query(`CREATE UNIQUE INDEX \`REL_85ca3582c13beb38be603599c5\` ON \`sessions_users\` (\`session_id\`)`);
        await queryRunner.query(`ALTER TABLE \`sessions_users\` ADD CONSTRAINT \`FK_85ca3582c13beb38be603599c5e\` FOREIGN KEY (\`session_id\`) REFERENCES \`sessions\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`sessions_users\` DROP FOREIGN KEY \`FK_85ca3582c13beb38be603599c5e\``);
        await queryRunner.query(`DROP INDEX \`REL_85ca3582c13beb38be603599c5\` ON \`sessions_users\``);
        await queryRunner.query(`ALTER TABLE \`sessions_users\` DROP INDEX \`IDX_85ca3582c13beb38be603599c5\``);
        await queryRunner.query(`ALTER TABLE \`sessions_users\` ADD CONSTRAINT \`FK_85ca3582c13beb38be603599c5e\` FOREIGN KEY (\`session_id\`) REFERENCES \`sessions\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`sessions\` DROP COLUMN \`second_player_field\``);
        await queryRunner.query(`ALTER TABLE \`sessions\` ADD \`second_player_field\` varchar(5120) NOT NULL DEFAULT '[[]]'`);
        await queryRunner.query(`ALTER TABLE \`sessions\` DROP COLUMN \`first_player_field\``);
        await queryRunner.query(`ALTER TABLE \`sessions\` ADD \`first_player_field\` varchar(5120) NOT NULL DEFAULT '[[]]'`);
    }

}
