import {ApiProperty} from "@nestjs/swagger";
import {SessionEntity} from "../session.entity";

export namespace FindSessionDto {
	export class FindSessionRequest {
		@ApiProperty()
		public playerId: number;

		constructor(playerId: number) {
			this.playerId = playerId;
		}
	}

	export class FindSessionResponse {
		@ApiProperty()
		public status: boolean;

		@ApiProperty()
		public errorMessage?: string;

		constructor(status: boolean, errorMessage?: string) {
			this.status = status;
			this.errorMessage = errorMessage;
		}
	}
}