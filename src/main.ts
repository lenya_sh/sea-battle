import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { CustomValidationPipe } from './pipes/validation.pipe';
import express from "express";
import { CustomExceptionFilter } from "./exceptions/CustomException.filter";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.use(express.json({limit:'8mb'}));
  app.useGlobalPipes(new CustomValidationPipe());
  app.useGlobalFilters(new CustomExceptionFilter());

  const config = new DocumentBuilder()
    .setTitle('app')
    .setDescription('API для sea-battle')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);

  //app.useWebSocketAdapter(new WsAdapter(app))
  await app.listen(process.env['APP_PORT_1']!, '0.0.0.0');

  console.log('=====SERVER ADDRESS=====');
  console.log(await app.getUrl());
}
bootstrap();