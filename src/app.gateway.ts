import {
	OnGatewayConnection,
	OnGatewayDisconnect,
	OnGatewayInit,
	SubscribeMessage,
	WebSocketGateway, WebSocketServer
} from "@nestjs/websockets";
import { Server } from 'ws';

@WebSocketGateway(3001)
export class AppGateway
	implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
	@WebSocketServer()
	private server: Server | undefined;
	wsClients: any[] = [];

	@SubscribeMessage('send_message')
	async handleSendMessage(client: any, payload: any): Promise<void> {
		console.log('==========');
		console.log(9879789);
		
		console.log('=====SEND MESSAGE HANDLE=====');
		console.log(payload);
		//await this.appService.createMessage(payload);
		this.server?.emit('recMessage', payload);
	}

	@SubscribeMessage('receiveMessage')
	async handleReceiveMessage(client: any, payload: any): Promise<void> {
		console.log('==========');
		console.log(9879789);
		console.log('=====RECEIVE MESSAGE=====');
		console.log(payload);
		//const newMessage = await this.messagesService.createMessage(payload);
		this.server?.emit('receiveMessage', payload);
	}

	afterInit(server: Server) {
		console.log('==========');
		console.log(123321);
		console.log(server);
		//Выполняем действия
	}

	handleDisconnect(client: any) {
		console.log('==========');
		console.log(9879789);
		console.log(`Disconnected: ${client.id}`);
		for (let i = 0; i < this.wsClients.length; i++) {
			if (this.wsClients[i] === client) {
				this.wsClients.splice(i, 1);
				break;
			}
		}
	}

	handleConnection(client: any, ...args: any[]) {
		console.log('==========');
		console.log(9879789);	
		console.log(`Connected ${client}`);
		this.wsClients.push(client);


		this.wsClients[0].emit('receiveMessage', '123321')

		this.wsClients[0].send('test', 'hello');
		this.wsClients[0].receive();
	}
}