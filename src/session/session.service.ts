import {Injectable, Logger} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {SessionEntity} from "./session.entity";
import {GetSessionByUserIdDto} from "./dto/getSessionByUserId.dto";
import {UsersService} from "../users/users.service";
import {SessionUsersEntity} from "./sessionUsers.entity";
import {Constants, GameCellStatus, GameSessionStatus, IGameFieldCell, IShipCoords, MoveResult} from "../constants";
import {CustomException} from "../exceptions/custom.exception";
import {CreateSessionDto} from "./dto/createSession.dto";
import {PlaceShipsDto} from "./dto/placeShips.dto";
import {GetSessionByIdDto} from "./dto/getSessionById.dto";
import {MakeMoveDto} from "./dto/makeMove.dto";
import {FindSessionDto} from "./dto/findSession.dto";
import {LookingForSessionEntity} from "./lookingForSession.entity";
import {Cron, CronExpression} from "@nestjs/schedule";
import {GetSessionStatusDto} from "./dto/getSessionStatus.dto";

@Injectable()
export default class SessionService {
	private logger: Logger;
	private cronInProcess: boolean;
	
	constructor(
		@InjectRepository(SessionEntity)
		private readonly sessionsRepository: Repository<SessionEntity>,
		@InjectRepository(SessionUsersEntity)
		private readonly sessionUsersRepository: Repository<SessionUsersEntity>,
		@InjectRepository(LookingForSessionEntity)
		private readonly lookingForSessionRepository: Repository<LookingForSessionEntity>,
		private readonly usersService: UsersService
	) {
		this.logger = new Logger('LookingPlayers CRON');
		this.cronInProcess = false;
	}

	public async findSession(request: FindSessionDto.FindSessionRequest): Promise<FindSessionDto.FindSessionResponse> {
		const player = await this.usersService.getUserById(request.playerId);
		
		try {
			const alreadyLookingPlayer = await this.lookingForSessionRepository.findOne({
				where: {
					userId: request.playerId,
					active: true
				}
			});
			if (!alreadyLookingPlayer)
				await this.getActiveSessionByUserId(new GetSessionByUserIdDto.GetSessionByUserIdRequest(player.id));
			
			return new FindSessionDto.FindSessionResponse(
				false,
				`У данного игрока уже есть активная игровая сессия`
			);
		}
		catch {
			const newLookingPlayer = await this.lookingForSessionRepository.create({
				userId: player.id
			});

			const savedLookingPlayer = await this.lookingForSessionRepository.save(newLookingPlayer);
			if (!savedLookingPlayer)
				throw CustomException.internalError('Ошибка при создании поиска игры');

			return new FindSessionDto.FindSessionResponse(true);
		}
	}
	
	@Cron(CronExpression.EVERY_30_SECONDS)
	public async checkLookingPlayers() {
		if (this.cronInProcess) {
			this.logger.log('Task are in process. SKIP');
			return;
		}
		
		this.cronInProcess = true;
		this.logger.log('START check looking players');
		try {
			await this.createSession();
		}
		catch (e) {
			this.logger.error(`Error in check looking players\nReason: ${e}`);
		}
		
		this.logger.log('STOP check looking players');
		this.cronInProcess = false;
	}
	
	public async createSession(): Promise<CreateSessionDto.CreateSessionResponse | undefined> {
		const [lookingPlayers, count] = await this.lookingForSessionRepository.findAndCount({
			where: {
				active: true
			},
			order: {
				createdAt: 'ASC'
			},
			relations: ["user"],
			take: 2
		});

		if (count < 2)
			return;
		
		lookingPlayers[0].active = false;
		lookingPlayers[1].active = false;
		
		const savedLookingPlayers = await this.lookingForSessionRepository.save(lookingPlayers);
		if (!savedLookingPlayers)
			throw CustomException.internalError('Ошибка при деактивации поиска у игроков');

		const newSession = this.sessionsRepository.create({
			status: GameSessionStatus.Start,
			firstPlayerField: await this.initGameField(),
			secondPlayerField: await this.initGameField(),
		});
		
		const savedSession = await this.sessionsRepository.save(newSession);
		if (!savedSession)
			throw CustomException.internalError('Ошибка при создании сессии');
		
		const sessionUsers = this.sessionUsersRepository.create({
			firstPlayerId: lookingPlayers[0].user.id,
			secondPlayerId: lookingPlayers[1].user.id,
			sessionId: savedSession.id,
		});
		
		const savedSessionUsers = await this.sessionUsersRepository.save(sessionUsers);
		if (!savedSessionUsers)
			throw CustomException.internalError('Ошибка при создании связи сессии с пользователями');
		
		return new CreateSessionDto.CreateSessionResponse(savedSession);
	}
	
	public async getActiveSessionByUserId(request: GetSessionByUserIdDto.GetSessionByUserIdRequest)
		: Promise<GetSessionByUserIdDto.GetSessionByUserIdResponse> {
		await this.usersService.getUserById(request.userId);
		
		const acceptableStatuses: GameSessionStatus[] = [
			GameSessionStatus.Start,
			GameSessionStatus.FirstPlacedShips,
			GameSessionStatus.SecondPlacedShips,
			GameSessionStatus.FirstMove,
			GameSessionStatus.SecondMove,
			GameSessionStatus.Pause,
		]

		const sessionUsers = await this.sessionUsersRepository.createQueryBuilder('su')
			.leftJoinAndSelect('su.session', "ss")
			.where("su.first_player_id = :userId OR su.second_player_id = :userId ",
				{ userId: request.userId }
			).andWhere("ss.status IN (:...status)", { status: acceptableStatuses })
			.getOne();
		
		if (!sessionUsers)
			throw CustomException.invalidData(`Активная сессия с игроком ${request.userId} не найдена`)
		
		return new GetSessionByUserIdDto.GetSessionByUserIdResponse(sessionUsers.session);
	}
	
	public async getSessionStatus(request: GetSessionStatusDto.GetSessionStatusRequest)
		: Promise<GetSessionStatusDto.GetSessionStatusResponse> {
		await this.usersService.getUserById(request.userId);

		const sessionUsers = await this.sessionUsersRepository.createQueryBuilder('su')
			.leftJoinAndSelect('su.session', "ss")
			.where("su.first_player_id = :userId OR su.second_player_id = :userId ",
				{ userId: request.userId }
			).getOne();

		if (!sessionUsers)
			throw CustomException.invalidData(`Активная сессия с игроком ${request.userId} не найдена`)

		return new GetSessionByUserIdDto.GetSessionByUserIdResponse(sessionUsers.session);
	}
	
	public async getSessionById(request: GetSessionByIdDto.GetSessionByIdRequest)
		: Promise<GetSessionByIdDto.GetSessionByIdResponse> {
		const relations = request.expand ? ["sessionUsers"] : undefined;
		
		const session = await this.sessionsRepository.findOne(request.sessionId, {
			relations: relations
		});
		
		if (!session)
			throw CustomException.invalidData(`Сессия с id ${request.sessionId} не найдена`);
		
		return new GetSessionByIdDto.GetSessionByIdResponse(session);
	}

	public async placeShips(request: PlaceShipsDto.PlaceShipsRequest): Promise<PlaceShipsDto.PlaceShipsResponse> {
		if (request.coords.length < Constants.CountOfShips)
			throw CustomException.invalidData(`Количество кораблей должно равняться ${Constants.CountOfShips}`);
		
		await this.usersService.getUserById(request.playerId);
		const {session} = await this.getSessionById(new GetSessionByIdDto.GetSessionByIdRequest(
			request.sessionId,
			true
		));
		
		const acceptableStatuses:GameSessionStatus[] = [
			GameSessionStatus.Start,
			GameSessionStatus.FirstPlacedShips,
			GameSessionStatus.SecondPlacedShips
		]
		
		if (!acceptableStatuses.includes(session.status))
			throw CustomException.conflict('Некорректный статус сессии');
		
		if (session.sessionUsers.firstPlayerId == request.playerId && session.status == GameSessionStatus.FirstPlacedShips)
			throw CustomException.conflict(`Поле первого игрока уже заполнено`);

		if (session.sessionUsers.secondPlayerId == request.playerId && session.status == GameSessionStatus.SecondPlacedShips)
			throw CustomException.conflict(`Поле второго игрока уже заполнено`);
		
		if (request.playerId != session.sessionUsers.firstPlayerId 
			&& request.playerId != session.sessionUsers.secondPlayerId
		)
			throw CustomException.forbidden(`Игрок ${request.playerId} не причастен к данной сессии`);
		
		const isFirstPlayer: boolean = request.playerId == session.sessionUsers.firstPlayerId; 
		
		if (isFirstPlayer) {
			session.firstPlayerField = await this.createGameField(request.coords);
			
			if(session.status == GameSessionStatus.Start)
				session.status = GameSessionStatus.FirstPlacedShips;
			else
				session.status = GameSessionStatus.FirstMove;
		}
		else {
			session.secondPlayerField = await this.createGameField(request.coords);

			if(session.status == GameSessionStatus.Start)
				session.status = GameSessionStatus.SecondPlacedShips;
			else
				session.status = GameSessionStatus.FirstMove;
		}
		
		const savedSession = await this.sessionsRepository.save(session);
		if (!savedSession)
			throw CustomException.internalError('Ошибка при заполнении поля');

		await this.hideOpponentShips(
			isFirstPlayer ? savedSession.secondPlayerField : savedSession.firstPlayerField
		);
		
		return new PlaceShipsDto.PlaceShipsResponse(session);
	}
	
	public async makeMove(request: MakeMoveDto.MakeMoveRequest): Promise<MakeMoveDto.MakeMoveResponse> {
		await this.usersService.getUserById(request.playerId);
		const {session} = await this.getSessionById(new GetSessionByIdDto.GetSessionByIdRequest(
			request.sessionId, 
			true
		));
		
		if (session.status == GameSessionStatus.FirstMove && session.sessionUsers.firstPlayerId != request.playerId
		|| session.status == GameSessionStatus.SecondMove && session.sessionUsers.secondPlayerId != request.playerId
		)
			throw CustomException.conflict('Сейчас не ваш ход');
		
		const isFirstPlayerMove: boolean = session.status == GameSessionStatus.FirstMove;
		const result: MoveResult = await this.getResultOfMove(
			request.move,
			isFirstPlayerMove ? session.secondPlayerField : session.firstPlayerField
		);
		
		if (result == 'win')
			session.status = isFirstPlayerMove ? GameSessionStatus.FirstWon : GameSessionStatus.SecondWon;
		else
			session.status = isFirstPlayerMove ? GameSessionStatus.SecondMove : GameSessionStatus.FirstMove;
		
		const savedSession = await this.sessionsRepository.save(session);
		if (!savedSession)
			throw CustomException.internalError('Ошибка при сохранении результата хода')

		await this.hideOpponentShips(
			isFirstPlayerMove ? savedSession.secondPlayerField : savedSession.firstPlayerField
		);
		
		return new MakeMoveDto.MakeMoveResponse(result);
	}
	
	private async createGameField(coords: IShipCoords[]): Promise<IGameFieldCell[][]> {
		const result: IGameFieldCell[][] = await this.initGameField();

		for (const coordinate of coords) {
			if (coordinate.number > Constants.MaxNumberCoordinate || coordinate.number < 0)
				throw CustomException.invalidData(`Некорректная позиция строки ${coordinate.number}`);
			
			result[Constants.LetterCoordinate.indexOf(coordinate.letter)][coordinate.number].status 
				= GameCellStatus.ShipCell;
		}
		
		return result;
	}
	
	private async initGameField(): Promise<IGameFieldCell[][]> {
		const result: IGameFieldCell[][] = [];
		
		for (let i: number = 0; i < Constants.LetterCoordinate.length; i++) {
			result.push([]);
			
			for (let j: number = 0; j < Constants.MaxNumberCoordinate; j++) {
				result[i].push({
					coords: {
						letter: Constants.LetterCoordinate[i],
						number: j
					},
					status: GameCellStatus.AvailableCell
				})
			}
		}
		
		return result;
	}
	
	private async getResultOfMove(moveCoords: IShipCoords, otherPlayerField: IGameFieldCell[][]): Promise<MoveResult> {
		const letterIndex = Constants.LetterCoordinate.indexOf(moveCoords.letter);
		
		switch (otherPlayerField[letterIndex][moveCoords.number].status) {
			case GameCellStatus.AvailableCell:
				otherPlayerField[letterIndex][moveCoords.number].status = GameCellStatus.MissedShotCell;
				return 'miss';
			case GameCellStatus.ShipCell:
				otherPlayerField[letterIndex][moveCoords.number].status = GameCellStatus.HittedShotCell;
				let isWinMove = true;

				for (const line of otherPlayerField) {
					for (const cell of line) {
						if (cell.status == GameCellStatus.ShipCell) {
							isWinMove = false;
							break;
						}
					}
				}
				
				return isWinMove ? 'win' : 'kill';
			case GameCellStatus.HittedShotCell:
			case GameCellStatus.MissedShotCell:
				throw CustomException.invalidData('В эту клетку уже был произведен выстрел');
		}
	}
	
	private async hideOpponentShips(field: IGameFieldCell[][]) {
		for (const line of field) {
			for (const cell of line) {
				if (cell.status == GameCellStatus.ShipCell)
					cell.status = GameCellStatus.AvailableCell;
			}
		}
	}
}